# post送信テスト

これは、railsでpost送信をしてみるプロジェクトです。rails server -p 3333とかでServerを立ち上げてローカルホストでアクセスしてください。フォームに値を入力して、送信すると、画面が遷移してpost送信が成されたことを確認できると思います。


##ルーティング

こんな感じ、src1のフォームから、POST送信でsrc2へ飛ぶ

```rb

Rails.application.routes.draw do
  get  '/src2',    to: 'plans#src2'
  post '/src2',    to: 'plans#src2'
  root 'plans#src1'
end

```

## ビュー

/src2へPOSTで飛ぶ

```rb

<form action="/src2" method="POST">
  First name: <input type="text" name="fname"><br>
  Last name: <input type="text" name="lname"><br>
  <input type="submit" value="Submit">
</form>

```

## 注意点


CSRF保護が有効になってるとうまく動きません。CSRF保護を無効にするには、ApplicationControllerを次のように編集します。

```rb

class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

end

```