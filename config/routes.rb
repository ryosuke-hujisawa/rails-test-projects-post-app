Rails.application.routes.draw do
  get  '/src2',    to: 'plans#src2'
  get  '/src3',    to: 'plans#src3'
  get  '/dest',    to: 'plans#dest'
  post '/src2',    to: 'plans#src2'
  root 'plans#src1'
end
